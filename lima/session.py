"""Session class."""

import configparser
import logging
import logging.config
import os
import re
import sys
import yaml
from pathlib import Path

logger = logging.getLogger(__name__)


class Session():
    """Global session object."""

    def __init__(self, LOGGING_CONFIG, LIMA_CONFIG, LIMA_HOME, version):
        """Initialize."""
        # Define the most essential paths and values that don't depend on
        # anything else.
        install_path = Path(__file__).parent
        self.lima_version = version
        self.user = os.getenv("USER")
        self.lima_home = Path(LIMA_HOME).expanduser()
        self.init_folder = Path.cwd()
        self.config_path = install_path / "config"
        self.scripts_path = install_path / "scripts"

        # Renew .lima folder if it does not exist
        new_setup = not self.lima_home.is_dir()
        if new_setup:
            os.makedirs(self.lima_home)

        # Set up logging system once and for all
        self.config_file_logger = self.config_path / LOGGING_CONFIG
        with open(self.config_file_logger, "r") as f:
            logger_config = yaml.safe_load(f)
        self.logfile = self.lima_home / "lima.log"
        logger_config["handlers"]["logfile"]["filename"] = self.logfile
        logging.config.dictConfig(logger_config)
        sys.excepthook = self._custom_excepthook

        # Log if setup is renewed
        if new_setup:
            logger.debug("Setup renewed")

        # Read configuration, either from local or global config file
        param = configparser.ConfigParser(allow_no_value=True)
        self.config_file_global = self.config_path / LIMA_CONFIG
        self.config_file_user = self.lima_home / LIMA_CONFIG
        if self.config_file_user.is_file():
            try:
                param.read(self.config_file_user)
            except Exception as e:
                logger.warning(f"Cannot read local configuration file, "
                               f"reverting to global configuration ({e})")
                param.read(self.config_file_global)
                self.config_file_user = None
            else:
                logger.debug(f"Using local configuration at "
                             f"{self.config_file_user}")
        else:
            param.read(self.config_file_global)

        self._read_config(param)

        if self.LAST_JOB_ID:
            logger.debug(f"Running as job #{self.LAST_JOB_ID}")

        self.scratch_userspace = self.scratch_root / self.user
        self.scratch_folder = self.scratch_userspace

    def _read_config(self, param):
        """Read configuration file."""
        try:
            filenames = param["filenames"]
            self.experiment_definition_file = filenames["definition_file"]
            self.session_file = Path(param["filenames"]["session_file"])
            self.CP_folder = filenames["CP_folder"]
            self.CSV_folder = filenames["CSV_folder"]
            self.CSV_image = filenames["CSV_image"]
            self.CSV_objects = filenames["CSV_objects"]
            self.concat_image_name = filenames["concat_image_name"]
            self.concat_object_name = filenames["concat_object_name"]

            locations = param["locations"]
            self.scratch_root = Path(locations["scratch_root"])
            self.batch_data_file = locations["batch_data_file"]
            self.dest_converted = Path(locations["dest_converted"])
            self.dest_processed = Path(locations["dest_processed"])

            patterns = param["patterns"]
            self.LAST_JOB_ID = os.getenv(patterns["SLURM_VAR"])
            self.PATH_RE = re.compile(patterns["PATH"])
            self.EXPERIMENT_RE = re.compile(patterns["EXPERIMENT"])
            self.CP_RAWIMAGE = re.compile(patterns["CP_RAWIMAGE"])
            self.SUBFOLDER_PATTERN = patterns["SUBFOLDER"]
            self.GROUP_FOLDER_PATTERN = patterns["GROUP_FOLDER"]
            self.TARGET_FILE_PATTERN = patterns["TARGET_FILE"]
            self.RAW_IMAGE = patterns["RAW_IMAGE"]
            self.CONVERTED_IMAGE = patterns["CONVERTED_IMAGE"]
            self.autocontrast_ending = patterns["autocontrast_ending"]
            self.thumbnail_ending = patterns["thumbnail_ending"]

            images = param["images"]
            self.spots_per_array = int(images["spots_per_array"])
            self.offset = int(images["offset"])
            self.enhance_factor = float(images["contrast_enhancement_factor"])
            self.resize_factor = float(images["resize_factor"])

            self.metadata_files = list(param["metadata"])
        except Exception as e:
            logger.error(
                f"Missing or damaged sections in configuration file ({e})")
            sys.exit(1)

    @staticmethod
    def _custom_excepthook(exc_type, value, trace_back):
        """
        Override default exception hook.

        Replace the default exception hoow with a custom function to
        log uncaught exceptions.
        """
        if issubclass(exc_type, KeyboardInterrupt):  # Do not log interruption
            print("\n")                              # via Ctrl+C
            logger.info("Session aborted by user")
            return

        logger.critical("Uncaught exception, contact code maintainer!",
                        exc_info=(exc_type, value, trace_back))
        logging.shutdown()


def parse_arguments(session):
    """Define and get command line arguments."""
    import argparse
    from lima.commands import prep, post, setup

    parser = argparse.ArgumentParser(
        description="LiMA Analysis Suite",
        epilog="Use --help option with modules to get more information, "
        "e.g. lima prep --help.")
    parser.set_defaults(func=lambda x, y: parser.print_help())
    parser.add_argument(
        "--version", action="version",
        version=f"LiMA Analysis Suite {session.lima_version}")
    subparsers = parser.add_subparsers(title="modules", metavar="")

    parser_init = subparsers.add_parser("prep", help="Preprocess files")
    parser_init.set_defaults(func=prep)
    parser_init.add_argument(
        "INPUT", help="input folder that contains all images to be analyzed")
    parser_init.add_argument(
        "PIPELINE", help="pipeline file path for CellProfiler 2.2.0")
    parser_init.add_argument(
        "--spots", type=int, default=session.spots_per_array,
        help="spot density (default: %(default)d)")
    parser_init.add_argument(
        "--def", dest="expdef", default=session.experiment_definition_file,
        help="name of the experiment definition file (default: %(default)s)")
    parser_init.add_argument(
        "--scratch", dest="scratch", default=session.scratch_root, type=str,
        help="Root directory for scratch space allocation "
        "(default: %(default)s)")

    parser_post = subparsers.add_parser("post", help="Postprocess files")
    parser_post.set_defaults(func=post)
    parser_post.add_argument(
        "--no-concat", dest="no_concat", action="store_true",
        help="Skip concatenation of CSV files")
    parser_post.add_argument(
        "--no-thumb", dest="no_thumb", action="store_true",
        help="Skip creating thumbnails for Inspector")
    parser_post.add_argument(
        "--no-save", dest="no_save", action="store_true",
        help="Do not destage files")
    parser_post.add_argument(
        "--no-cleanup", dest="no_cleanup", action="store_true",
        help="Do not clean up scratch space")

    parser_setup = subparsers.add_parser(
        "setup", help="Setup and maintenance",
        epilog="Prints current configuration if no arguments are passed.")
    parser_setup.set_defaults(func=setup)
    group_config = parser_setup.add_mutually_exclusive_group()
    parser_setup.add_argument(
        "--clean-scratch", dest="clean_scratch", action="store_true",
        help="clean up scratch userspace")
    group_config.add_argument(
        "--renew-config", dest="renew_config", action="store_true",
        help="renew local configuration file from source")
    group_config.add_argument(
        "--remove-config", dest="remove_config", action="store_true",
        help="remove local configuration file")
    parser_setup.add_argument(
        "--show-log", dest="show_log", action="store_true",
        help="print the current log file.")
    parser_setup.add_argument(
        "--session-info", dest="session_file", metavar="FILE",
        const=session.session_file, nargs="?",
        help="show session information or base configuration")
    parser_setup.add_argument(
        "-v", dest="verbose", action="count", default=0,
        help="increase verbosity when displaying session info")

    return parser.parse_args()


def print_session_info(session, verbose=0):
    """Display session information."""
    from lima.data import print_experiment_table

    config_items = session.__dict__
    exp_dict = config_items.pop("exps", None)
    file_list = config_items.pop("files", None)

    print("{:*^80}".format(" Begin session info "))
    for key, value in config_items.items():
        print(f"{key: >26} = {value}")

    if verbose > 0:
        print("\nExperiments:")
        print_experiment_table(exp_dict)

    if verbose > 1:
        print("Files:")
        for image_file in file_list:
            print(f"{image_file.source} "
                  f"({image_file.source_size / 1E+6:.1f} MB)")
            print(f" -> {image_file.target} "
                  f"({image_file.target_size / 1E+6:.1f} MB)")

    print("{:*^80}".format(" End session info "))


def update_session_info(session):
    """Write session info to file."""
    import pickle
    try:
        with open(session.session_file, "wb") as f:
            pickle.dump(session, f, -1)
    except Exception as e:
        logger.error(f"Cannot write session file ({e})")
        sys.exit(1)
    else:
        logger.debug("Session saved")


def confirm_jobID(session):
    """Confirm that the script is run on a cluster node."""
    if not session.LAST_JOB_ID:
        logger.error("LiMA Analysis Suite must be run on a cluster node, "
                     "please use slima command.")
        sys.exit(1)
