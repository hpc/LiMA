#!/usr/bin/bash

#SBATCH -n1
#SBATCH -t 5:00

module purge
module load CellProfiler/2.2.0-foss-2016b-Python-2.7.12
srun cellprofiler -c -r -b -p {pipeline} --file-list={filelist}
