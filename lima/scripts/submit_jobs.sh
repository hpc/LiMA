#!/usr/bin/bash

#SBATCH -n1
#SBATCH --time=15:00
#SBATCH --tmp=5G
#SBATCH --array=1-{array_end}%200
#SBATCH --mail-type=FAIL,ARRAY_TASKS

srun ./run_cellprofiler.sh "$SLURM_ARRAY_TASK_ID"
