#!/usr/bin/bash

module purge
module load CellProfiler/2.2.0-foss-2016b-Python-2.7.12
cellprofiler -c -r -b -p {batch_data_file} -o "$TMPDIR" -f "$1" -l "$1" & wait $!
cp -rfvn "$TMPDIR"/* {output}/
