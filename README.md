# LiMA Analysis Suite

This is a file transfer, image compression and analysis tool designed for LiMA experiments performed on the cube4 system.

For more information, see our [Wiki][25366b6f].

  [25366b6f]: https://git.embl.de/grp-gavin/LiMA/wikis/home "LiMA Wiki"
